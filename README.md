# Audacious Skins Audacious Skins Winamp Style Collection

Audacious Skins Winamp Style Collection / Συλλογή Θεμάτων σε στύλ Winamp για Audacious

![Screenshot](https://i.imgur.com/uQ4FNwel.png)

Credits to my friend https://gitlab.com/gnutechie

- Downloads in many compressed types prepared by GitLab as shown below / Τα Downloads σε συμπιεσμένη μορφή τα κατεβάζετε απευθείας από τι GitLab όπως φαίνεται στην εικόνα:

- ![Screenshot](https://i.imgur.com/DFhafJ6.png)

- Download and extract it to a folder / Τα "κατεβάζετε" και τα εξάγετε μετά σε έναν φάκελο.

- Open terminal in the extracted folder and execute: / Ανοίγετε τερματικό στον φάκελο που έχουν αποσυμπιεστεί και εκτελείτε:

- ```sudo cp -R */ /usr/share/audacious/Skins/```
