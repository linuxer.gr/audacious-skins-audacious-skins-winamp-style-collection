                                                          ____    _____
           _________  _ __  __  __   __ __   ____        / __/_ _|__  /
          / ___/ __ \/ `_ \/ / / /  / _` _`\/ __ \______/ /_/ //_//_ <
         (__  ) /_/ / / / / /_/ /  / // // / /_/ /_____/ __/>  <___/ /
        /____/\____/_/ /_/\__, /  /_//_//_/ ,___/     / / /_//_/____/
                         /____/          /_/         /_/     v2.22


Thursday 2/12/99 19:06

Winamp skin for Winamp 2.5 by Obi McNally 
� Copyright 1999 and all that crap

-= Available from http://www.rpi.net.au/~ballly =- (yes 3 l's)
-= and bally.webjump.com =-

also from
- http://www.1001winampskins.com/skin_details.html?id=224 (rated no. 5 :)
- http://www.skintacular.com - also known as skinz.org - look in
  hi-fi skins

LCD version also sitting in skinz.org and 1001winampskins


-= Installation =-

The zip file should have 19 files in it including this one.
Unzip all the files into ur /winamp/skins directory (it will create a 
sub-dir called SONY MP-FX3) and in winamp hit alt-s, choose sony
mp-fx3 and off u go. 

-= Recommended VIS options =-

Spectrum Analyser
Fire style 
Thin bands
70fps refresh rate
Analyser falloff full (right)
Line scope
Smooth VU

-= About =-

The displays are green, like backlit type green. i hope
it dont disturb anyone cos its too bright, if you dont like
it go an download the LCD version from skinz.org or 1001winampskins
It's made to look neat and tidy, none of this out of alignment 
bullshit, if u find any glitches or problems or if u have 
any suggestions let me know and if i agree wif ya i'll change it :]
The whole skin was made using PSP5 and 6 (www.jasc.com) and with the
help of a couple lil plugins (lil - not this eyecandy crap, i do 
things the proper way)

-= Features =-

- 4 windows, all sony style, no winamp default ugly graphics ;]
- SONY MP-FX3 Main window
- SONY MP-EQ3 Graphic Equaliser
- SONY MP-PL3 Playlist Editor
- SONY MP-MB3 Minibrowser
- All windowshade modes, fully functional
- All buttons have depression stages (ok not the slider buttons,
  they dont really need em)
- Backgrounds of skin graphics are yellow to make for easier
  editing (not that u need to edit it :])
- Customised font and vis, best ever :]
- Realistic LED time display <- this changed somewhat...looks
  better though
- Metallic slider buttons
- Like 99% orignal content, the only thing i didnt make was the sony
  logo which is from the Sony 4000 skin

-= Changes since v2.2 (14/9/99) =-

I have started the changes again from scratch, this time i will put 
dates.

10/10/99 - 	Well due to popular demand i have removed the text 
		from the bottom left hand corners (hmmm)
	 -	Thanks to Nick i discovered an easter egg that changes 
		the titlebar image, to see this type "nullsoft" slowly 
		in the main window, and remember to press escape after 
		the L's
	 -	Added the idle stop indicator to the main display 
		(it used to be there but somehow it dissapeared)
20/9/99 - 	The battery indicator (like on most sony portable 
		products)
		it doesnt actually change but it does make it look 
		more realistic
	-	Colourful EQ sliders (green-yellow-orange-red) i like 
		this as	it looks like lil LED lights


have fun email me with comments or shit like that hehe
i'm @ ballly@hotmail.com

ok so now i got the skin up at skinz.org and 1001winampskins.com
and its goin ok...hope to see its luck progress - ranked 11th
at 1001winampskins.com with around 35,000 downloads

                     __    _                             ____
        by:   ____  / /_  (_)  __ __   ______ __ _____  / / /_  __
             / __ \/ __ \/ /  / _` _`\/ ___/ `_ \\___ \/ / / / / /
            / /_/ / /_/ / /  / // // / /__/ / / / __  / / / /_/ /
            \____/\____/_/  /_//_//_/\___/_/ /_/\__,_/_/_/\__, /
                   icq: 9835716                          /____/
