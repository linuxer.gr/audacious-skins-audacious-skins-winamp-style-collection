Sepish - Skin For Winamp

version: 1.2 (06-02-2000)
author: Kauko Mikkonen alias KaMi

Thank you for downloading this skin.

Transparent skin for Winamp,  I just wanted to know is it possible to make a round skin... Yes, but some parts will be missing... I recommend you to put the main window on top and the equalizer below it so the skin looks round.

What's new? Skinned AVS plug-in,  if you don't already have the plug-in get it: 
http://winamp.com/customize/ 

BTW... if you are making a skin and want to know how to make a skin transparent open the region.txt in this skin (you need Winzip or some another program like that to open the zip file), I made some notes which may help you to understand that list of numbers...

If you have any comments, questions 
or suggestions email me or write it down at skinz.org
http://skintacular.com/skins.php3?skin=Sepish&area=winamp2 (you need to register to write a comment...)

Enjoy!

__KaMi__
Email:kaum@mbnet.fi
Homepage: http://koti.mbnet.fi/kaum/
http://koti.mbnet.fi/kaum/skins/sepishe.htm

