      +---------------------------------------+
      |  Audio LCARS Skin for WinAmp v2.x     |
      |  Bob Zormeir  2/99   bobz@serv.net    |
      +---------------------------------------+


This is a skin for WinAmp v2.0 of WinAmp, but should be
compatible with earlier versions. Get the latest version
of Winamp so you don't have to unzip this file to use it. 
(Thanks to Izaro Lopez-Garcia for his contributions.)


Installation: v2.x and earlier
------------
1. Copy or move AudioLCARS.zip to the WinAmp Skins directory. 
2. Unzip the file to generate the 'AudioLCARS' directory and 
   necessary files for the 'AudioLCARS' skin.
3. Launch WinAmp, and type Alt-S for the Skin Browser.
4. Double-click on 'AudioLCARS.'

Installation: v2.4 and later
------------
1. Copy or move AudioLCARS.zip to the WinAmp Skins directory. 
2. Launch WinAmp, and type Alt-S for the Skin Browser.
3. Double-click on 'AudioLCARS.'