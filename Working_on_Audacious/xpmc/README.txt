XPM Classic for WinAmp - Release Notes
======================================

28. July 2005 - Final Release

created by Jim-Phelps (Rafael Czerwinski, design@jim-phelps.de)

Credits
-------
This skin is a port of the XPMC MSStyle by bOse [1]
Permission granted.

This skin fully compatible to all skinnable versions of WinAmp.
Some additional plugins are skinned aswell (Amarok, VidAmp, MikroAmp).

Please don't modify or distribute this skin without my permission.

Thanks
------
gfxOasis community

Links
-----
[1] http://www.deviantart.com/view/20901820/