29,31,34, // color 0 = light grey
51,54,57, // color 1 = lighter grey for dots
86,0,45, // color 2 = top of spec
239,0,177 // 3
239,0,177 // 4
196,0,110, // 5
196,0,110, // 6
160,0,90, // 7
160,0,90,, // 8
127,0,71, // 9
127,0,71, // 10
97,0,54, // 11
97,0,54, // 12
75,0,41, // 13
75,0,41, // 14
86,0,45,  // 15
86,0,45,  // 16
86,0,45,   // 17 = bottom of spec
210,210,210, // 18 = osc 1
174,174,174, // 19 = osc 2 (slightly dimmer)
138,138,138, // 20 = osc 3
102,102,102,  // 21 = osc 4
66,66,66,  // 22 = osc 5
229,0,131, // 23 = analyzer peak dots
