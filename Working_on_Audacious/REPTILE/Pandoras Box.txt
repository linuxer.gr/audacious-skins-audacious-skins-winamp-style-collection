--EUPHORBIA TRIGONA--
---------REPTILE v.1.0---------

Creator : Marek Wojtal
E-mail : Euphorbia@giga4u.de

URL:
http://homepages.compuserve.de/marekwojtal

Skinname : Reptile
Version : 1.0
Origin releasedate : 19th of July 2000
Updated : no
Colourdepth : highcolour

TERMS OF USING REPTILE-SKIN :

It is prohibited to use elements of this skin for other skin-projects.
It is not allowed to publish REPTILE (or other) skin without my permission. 
It is not allowed to sell it in a package or separately.
It is not allowed to add any files or signs to REPTILE without my permission.
It is forbidden to delete or change any of the following files in this zip-file (or wsz-file) :
avs.bmp
balance.bmp
cButtons.bmp
eq_ex.bmp
eqMain.bmp
main.bmp
Mb.bmp
monoSter.bmp
nums_ex.bmp
playPaus.bmp
plEdit.bmp
plEdit.txt
posBar.bmp
shufRep.bmp
text.bmp
titlebar.bmp
viscolor.txt
volume.bmp
Pandoras Box.txt (this "readme" file)
These Terms underlie the primary E.T.F.-Conditions at 
http://homepages.compuserve.de/marekwojtal

Other Skins done by me
(in order of appereance) :
1. Euphorbia Trigona
2. 1914
3. SiMONE
4. WROCLAW
5. Unison Brainstormed
6. REPTILE

CONTACT-AREA :

You can drop me a line in english, german or polish to :
Euphorbia@giga4u.de

Best Regards & THNX for interest
Marek Wojtal