Remnant v1.0 - Peacemaker Graphics

copyright (c) 2010

A few years in the making I've produced another organic-alien meshed skin
with a simple clean flow of metal on a darkened based theme.

As usual the boundaries of the interface has challenged the outcome of the
design but in fairness I am very satisfied with how it all shaped together.

Thanks for downloading, and please check out these supported websites:

www.peacemakergfx.com
www.breedart.org
www.1001skins.com

- Michael.