=/\= Outpost 10F Winamp Skin Version 1.2 =/\=


How to use this skin:

First remember in which directory you've put the .zip file,
or where you've unzipped it in. Then start Winamp and hit Alt-S,
then choose the "O10F" skin and press close. If it's not listed,
choose the "Set Skin Directory..." option, and select the directory
where you put the file first. 

Enjoy!

This skin contains files for the Winamp player,
equalizer, playlist and mini browser.
It was tested on Winamp V2.21.

For more information about Nullsoft Winamp, visit www.winamp.com
For more information about Outpost 10F, visit www.outpost10f.com

______________________________________________________________
This skin is based on Outpost 10F, an interactive Star Trek
chat. The skin may be distributed freely, as long as this
file comes with it. It may not be used for commercial purposes
in any way. 

By JKAD (JKAD@outpost10f.com)
for the Outpost 10F Department of Engineering
(www.outpost10f.com/departments/engineering/)

First created May 16, 1999
Version 1.2 created June 9, 1999

New in this version: Mini Browser skinned