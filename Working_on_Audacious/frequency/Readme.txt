___ FREQUENCY ___

 by Stephen Moss

 release date: April 11, 2000

__________________
NOTES

This is the companion Winamp skin for New Line Cinema's new film, "Frequency."

It is inspired by the Sullivan family's ham radio, central to the film.

I hope you dig it!

__________________
COPYRIGHT

all graphics and design � 2000 by Stephen Moss.

The motion picture "Frequency"  � 2000 by New Line Cinema.

__________________
THANKS

Everyone with Nullsoft, AOL, and New Line.

__________________
CONTACT
  
If you wish, you can check out my homepage.  Dig it at:
http://www.rit.edu/~slm2401/

Drop me a line.  You can reach me at:
steveo@israel.crosswinds.net

__________________