Aqua X Winamp Skin V1.1
by Freddy Leitner / Digital Dreamer [www.dreamer.de]



Description
----------------------------------------------------------------------
In 2000, just after Apple presented their Aqua GUI for their upcoming
MacOS X, I decided to inject their wonderful layout into a Winamp
skin. For the most part because I wanted to have a Winamp skin that's
as clean and tidy as the Aqua environment. And for a small part
because I was scared off the other Aqua based skins that weren't too
well done.

Until now the skin was downloaded more than 180.000 times and I still
receive very rewarding mails from all around the world. It can be
found on many different sites, all stating it is indeed the closest
Aqua GUI skin rendition there is for Winamp 2.x. I hope to continue
the success of this skin by adding skins for the new windows
introduced by Winamp 2.9.

Have fun everyone, and thanks for using this skin.

- Freddy
  April 13th, 2003



Version History
----------------------------------------------------------------------
v1.00, November 2000
   Initial release
v1.01, August 2002
   Non-public release
   [-] Fixed the font display using an underscore instead of a dot
v1.1, April 2003
   [*] Skinned the new windows introduced with Winamp 2.9.
   [-] Fixed the MiniBrowser status display, now integrates with the
       color theme.


Known issues
----------------------------------------------------------------------
I'm certainly not satisfied with the way the Media Library window came
along. Let aside the more obvious bugs it is practically impossible to
integrate any more complex elements than the standard rectangle button
and the plain colored background into the new "generic window"
skinning system. Alas, I tried my best.

As far as the song title display and playlist font is concerned, for a
short time I tried another font. It would be a little sharper, but it
doesn't blend in with the surrounding layout. So I changed it back to
the old font.
